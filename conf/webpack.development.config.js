var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var config = require('webpack-config');

module.exports = new config.Config().extend("conf/webpack.base.config.js").merge({
  entry: './src/devSrc/index.tsx',
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/devSrc/index.html'
    })
  ],
  devServer: { inline: true },
  devtool: 'inline-source-map',
  module: {
    rules: [
      {
        test: /\.less$/,
        use: [{
          loader: "style-loader" // creates style nodes from JS strings
        }, {
          loader: "css-loader" // translates CSS into CommonJS
        }, {
          loader: "less-loader" // compiles Less to CSS
        }]
      }
    ]
  }
})