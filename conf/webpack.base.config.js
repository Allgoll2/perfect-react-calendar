var webpack = require('webpack');
var path = require('path');
var config = require('webpack-config');

module.exports = new config.Config().merge({
  entry: './src/index.tsx',
  output: {
    path: __dirname + '/../build',
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: [
          'ts-loader'
        ]
      }
    ]
  }
})