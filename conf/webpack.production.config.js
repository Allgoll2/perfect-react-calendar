var webpack = require('webpack');
var config = require('webpack-config');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

var extractLess = new ExtractTextPlugin({
  filename: "calendar.css"
});

module.exports = new config.Config().extend("conf/webpack.base.config.js").merge({
  entry: './src/index.tsx',
  plugins: [
    extractLess
  ],
  module: {
    rules: [
      {
        test: /\.less$/,
        use: extractLess.extract({
          use: [{
            loader: "css-loader" // translates CSS into CommonJS
          }, {
            loader: "less-loader" // compiles Less to CSS
          }]
        })
      }
    ]
  }
})