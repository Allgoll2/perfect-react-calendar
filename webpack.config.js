var config = require('webpack-config');

config.environment.setAll({
  env: function() { return process.env.NODE_ENV || 'development' }
});

module.exports = new config.Config().extend('conf/webpack.[env].config.js');